import { Component, ElementRef, ViewChild } from '@angular/core';
import { DialogService } from '../service/dialog/dialog.service';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  @ViewChild('dataToModal', { static: true })
  public dataToModal: ElementRef;

  constructor(private dialogService: DialogService) {
  }

  public addModalComponentWithoutData() {
    this.dialogService.open(ModalComponent);
  }

  public addModalComponentWithData() {
    const msgData = this.dataToModal.nativeElement.value;
    this.dialogService.open(ModalComponent, {
      data: {
        msg: msgData ? msgData : `I'm the default message from HomeComponent`,
      },
    });
  }
}
