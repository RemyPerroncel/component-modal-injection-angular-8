import {
  ApplicationRef,
  ComponentFactoryResolver,
  ComponentRef,
  EmbeddedViewRef,
  Injectable,
  Injector,
  Type,
} from '@angular/core';
import { DialogConfig } from './dialog-config.model';
import { DialogInjector } from './dialog.injector';

@Injectable({providedIn: 'root'})
export class DialogService {

  public dialogComponentRef: ComponentRef<any>;

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private appRef: ApplicationRef,
              private injector: Injector) {
  }

  public open(component: Type<any>, config?: DialogConfig) {
    this.appendDialogComponentToBody(component, config);
  }

  public close() {
    this.removeDialogComponentFromBody();
  }

  private appendDialogComponentToBody(component: Type<any>, config?: DialogConfig) {
    console.log(config);
    const map = new WeakMap();
    if (config !== undefined) {
      map.set(DialogConfig, config);
    }

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
    const componentRef = componentFactory.create(new DialogInjector(this.injector, map));
    this.appRef.attachView(componentRef.hostView);

    const domElem = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    document.body.appendChild(domElem);

    this.dialogComponentRef = componentRef;
  }

  private removeDialogComponentFromBody() {
    this.appRef.detachView(this.dialogComponentRef.hostView);
    this.dialogComponentRef.destroy();
  }
}
