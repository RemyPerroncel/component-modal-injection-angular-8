import { Component } from '@angular/core';
import { DialogService } from '../service/dialog/dialog.service';
import { DialogConfig } from '../service/dialog/dialog-config.model';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent {

  public msg: string = undefined;

  constructor(private dialogService: DialogService,
              private configDialog: DialogConfig) {
    if (this.configDialog !== undefined && this.configDialog.data) {
      this.msg = this.configDialog.data.msg;
    }
  }

  public closeModal(): void {
    this.dialogService.close();
  }
}
