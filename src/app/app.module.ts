import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { ModalComponent } from './modal/modal.component';
import { DialogConfig } from './service/dialog/dialog-config.model';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    DialogConfig
  ],
  entryComponents: [
    ModalComponent
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
